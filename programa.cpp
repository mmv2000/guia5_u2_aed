#include <iostream>
#include <string.h>
#include <fstream>
#include "Avl.h"

using namespace std;

class Grafico {
    private:
        Nodo *arbol = NULL;

    public:
        // Constructor de la Clase Graficar.
        Grafico(Nodo *raiz) {
	        this->arbol = raiz;
        }

        // ofstream es el tipo de dato correspondiente a archivos en cpp (el llamado es ofstream &nombre_archivo).
        void recorre_Arbol(Nodo *p, ofstream &grafica) {
          string cadena = "\0";
 	        /* Se enlazan los nodos del grafo, para diferencia entre izq y der a cada nodo se le entrega un identificador*/
 	        if (p != NULL) {
 	        // Por cada nodo ya sea por izq o der se escribe dentro de la instancia del archivo.
 	          if (p->izq != NULL) {
 		           grafica << "\"" << p->dato << "\"->\"" << p->izq->dato << "\"[label=\"" << p->izq->FE << "\"];" << endl;
 	           }
 	          else {
              cadena = p->dato + "i";
 		          grafica << "\"" << cadena << "\" [shape=point];" << endl;
 		          grafica << "\"" << p->dato << "\"->" << "\"" << cadena << "\";" << endl;
 	          }

 	          if (p->der != NULL) {
 		           grafica << "\"" << p->dato << "\"->\"" << p->der->dato << "\"[label=\"" << p->der->FE << "\"];" << endl;
 	          }

 	          else {
              cadena = p->dato + "d";
              grafica << "\"" << cadena << "\" [shape=point];" << endl;
 		          grafica << "\"" << p->dato << "\"->" << "\"" << cadena << "\";" << endl;
 	        }

 	        // Se realizan los llamados tanto por la izquierda como por la derecha para la creación del grafo.
 	          recorre_Arbol(p->izq, grafica);
 	          recorre_Arbol(p->der, grafica);
 	        }
        }

        void crea_Grafico() {
          ofstream grafica;
	        // Se abre/crea el archivo datos.txt, a partir de este se generará el grafo.
	        grafica.open("datos.txt");
	        // Se escribe dentro del archivo datos.txt "digraph G { ".
	        grafica << "digraph G {" << endl;
	        // Se pueden cambiar los colores que representarán a los nodos, para el ejemplo el color será verde.
	        grafica << "node [style=filled fillcolor=cyan];" << endl;
          grafica << "nullraiz [shape=point];" << endl;
          grafica << "nullraiz->\"" << this->arbol->dato << "\" [label=" << this->arbol->FE<< "];" << endl;
	        // Llamado a la función recursiva que genera el archivo de texto para creación del grafo.
	        recorre_Arbol(this->arbol, grafica);
	        // Se termina de escribir dentro del archivo datos.txt.
	        grafica << "}" << endl;
	        grafica.close();
	        // Genera el grafo.
	        system("dot -Tpng -ografo.png datos.txt &");
	        system("eog grafo.png &");
        }
};

void menu(Avl *arbol_nuevo, Nodo *raiz){
  int op;
  int altura;
  string ID_proteina;
  string ID_nuevo;

  // Menú
  do {
    cout << "\t:::MENU:::" << endl;
    cout << "1. Ingresar ID" << endl;
    cout << "2. Eliminar ID" << endl;
    cout << "3. Modificar ID" << endl;
    cout << "4. Cargar algunos ID proteinas" << endl;
    cout << "5. Cargar todos los ID proteinas" << endl;
    cout << "6. Mostrar grafo" << endl;
    cout << "0. Salir" << endl;

    cout << "Ingrese su opcion" << endl;
    cin >> op;

    switch (op) {
      case 1:
        system("clear");
        cout << "Ingrese dato:" << endl;
        cin >> ID_proteina;
        // Crea el nodo y se ingresa al arbol
        arbol_nuevo->insertarNodo(&raiz, &altura, ID_proteina);
        break;

      case 2:
        // Eliminación del dato ingresado
        system("clear");
        cout << "Ingrese el ID a eliminar:" << endl;
        cin >> ID_proteina;
        arbol_nuevo->Buscar_nodo(raiz, ID_proteina);
        arbol_nuevo->eliminarNodo(&raiz, &altura, ID_proteina);
        break;

      case 3:
      system("clear");
        // Se elimina un nodo y se agrega uno nuevo
        cout << "Ingrese el ID a eliminar:" << endl;
        cin >> ID_proteina;
        arbol_nuevo->eliminarNodo(&raiz, &altura, ID_proteina);
        cout << "Ingrese un ID nuevo:" << endl;
        cin >> ID_nuevo;
        arbol_nuevo->insertarNodo(&raiz, &altura, ID_nuevo);
        break;

      case 4:
      // Cargar una cantidad menor de datos
        cout << "Cargando IDs de algunas proteinas:" << endl;
        arbol_nuevo->Leer_algunos(&raiz, &altura);
        break;

      case 5:
      // Cargar todos los IDs de proteínas
        cout << "Cargando IDs de algunas proteinas:" << endl;
        arbol_nuevo->Leer_todos(&raiz, &altura);
        cout << "Se cargaron todos los IDs al arbol" << endl;
        break;

      case 6:
        // Grafo del arbol
        Grafico *g = new Grafico(raiz);
        g->crea_Grafico();
        break;
    }
  } while(op != 0);
}

int main(int argc, char const *argv[]) {
  Avl *arbol_nuevo = new Avl();
  Nodo *raiz = NULL;

  menu(arbol_nuevo, raiz);

  return 0;
}
