#include <iostream>
#include <string.h>

using namespace std;

/* Estructura del nodo */
typedef struct _Nodo {
  string dato;
  // Dato que calcula la altura del subarbol derecho menos la altura del subarbol izquierdo
  int FE;
  struct _Nodo *izq;
  struct _Nodo *der;
} Nodo;
