#include <iostream>
#include <string.h>
#include <fstream>
#include "Avl.h"

using namespace std;

Avl::Avl() {

}

// Crea nuevo nodo
Nodo* Avl::crea_Nodo(string ID){
  Nodo *nodotemp = new Nodo;

  // Valor para el nodo
  nodotemp->dato = ID;

  // Subarboles derecho e izquierdo
  nodotemp->izq = NULL;
  nodotemp->der = NULL;

  return nodotemp;
}
/*
// Rotación de las ramas izquierdas
void Avl::rotacion_II(Nodo *&raiz, bool altura, Nodo *nodo1){
  raiz->izq = nodo1->der;
  nodo1->der = raiz;

  // Identifica valor actual del factor de cambio de los nodos modificados
  switch (nodo1->FE) {
    case 0:
      raiz->FE = -1;
      nodo1->FE = 1;
      altura = false;
      break;

    case -1:
      raiz->FE = 0;
      nodo1->FE = 0;
      altura = false;
      break;
  }

  // Valor raiz nuevo asignado
  raiz = nodo1;
}

// Rotación ramas izquierdas y luego derechas
/*
void Avl::rotacion_ID(Nodo *&raiz, Nodo*nodo1, Nodo *nodo2){
  nodo2 = nodo1->der;
  raiz->izq = nodo2->der;
  nodo2->der = raiz;
  nodo1->der = nodo2->izq;
  nodo2->izq = nodo1;

  if(nodo2->FE == -1){
    raiz->FE = 1;
  }
  else{
    raiz->FE = 0;
  }

  if(nodo2->FE == 1){
    nodo1->FE = -1;
  }
  else{
    nodo1->FE = 0;
  }

  raiz = nodo2;
  nodo2->FE = 0;
}
*/


void Avl::Reestructura_der(Nodo **raiz, int *altura){
  Nodo *nodo;
  Nodo *nodo1;
  Nodo *nodo2;

  nodo = *raiz;

  if(*altura == true){

    /*Verifica el valor del factor de equilibrio
     para identificar si se realizara una reestructuración y cual */
    switch (nodo->FE) {
      case -1:
        nodo->FE = 0;
        break;

      case 0:
        nodo->FE = 1;
        *altura = false;
        break;

      case 1:
        nodo1 = nodo->der;
        // Rotación a realizar dependiendo del valor del factor de equilibrio
        if(nodo1->FE >= 0){
          nodo->der = nodo1->izq;
          nodo1->izq = nodo;

          switch (nodo1->FE) {
            case 0:
              nodo->FE = 1;
              nodo1->FE = -1;
              *altura = false;
              break;

            case 1:
              nodo->FE = 0;
              nodo1->FE = 0;
              *altura = false;
              break;
          }
          nodo = nodo1;
        }

        else{
          nodo2 = nodo1->izq;
          nodo->der = nodo2->izq;
          nodo2->izq = nodo;
          nodo1->izq = nodo2->der;
          nodo2->der = nodo1;

          if(nodo2->FE == 1){
            nodo->FE = -1;
          }
          else{
            nodo->FE = 0;
          }
          if(nodo2->FE == -1){
            nodo1->FE = 1;
          }
          else{
            nodo1->FE = 0;
          }

          nodo = nodo2;
          nodo2->FE = 0;
        }
        break;
    }
  }
  *raiz = nodo;
}

/*
void Avl::rotacion_DD(Nodo *&raiz, bool altura, Nodo *nodo1){
  raiz->der = nodo1->izq;
  nodo1->izq = raiz;

  switch (nodo1->FE) {
    case 0:
      raiz->FE = 1;
      nodo1->FE = -1;
      altura = false;
      break;

    case 1:
      raiz->FE = 0;
      nodo1->FE = 0;
      altura = false;
      break;
  }

  raiz = nodo1;
}

void Avl::rotacion_DI(Nodo *&raiz, Nodo *nodo1, Nodo *nodo2){
  nodo2 = nodo1->izq;
  raiz->der = nodo2->izq;
  nodo2->izq = raiz;
  nodo1->izq = nodo2->der;
  nodo2->der = nodo1;

  if(nodo2->FE == 1){
    raiz->FE = -1;
  }
  else{
    raiz->FE = 0;
  }

  if(nodo2->FE == -1){
    nodo1->FE = 1;
  }
  else{
    nodo1->FE = 0;
  }

  raiz = nodo2;
  nodo2->FE = 0;
}

*/

void Avl::Reestructura_izq(Nodo **  raiz, int *altura){
  Nodo *nodo;
  Nodo *nodo1;
  Nodo *nodo2;

  nodo = *raiz;

  if(*altura == true){

    /*Verifica el valor del factor de equilibrio
     para identificar si se realizara una reestructuración y cual*/
    switch (nodo->FE) {
      case 1:
        nodo->FE = 0;
        break;

      case 0:
        nodo->FE = -1;
        *altura = false;
        break;

      case -1:
        nodo1 = nodo->izq;
        // Rotación a realizar dependiendo del valor del factor de equilibrio
        if(nodo1->FE <= 0){
          nodo->izq = nodo1->der;
          nodo1->der = nodo;

          switch (nodo1->FE) {
            case 0:
              nodo->FE = -1;
              nodo1->FE = 1;
              *altura = false;
              break;

            case -1:
              nodo->FE = 0;
              nodo1->FE = 0;
              *altura = false;
              break;
          }
          nodo = nodo1;
        }

        else{
          nodo2 = nodo1->der;
          nodo->izq = nodo2->der;
          nodo2->der = nodo;
          nodo1->der = nodo2->izq;
          nodo2->izq = nodo1;

          if(nodo2->FE == -1){
            nodo->FE = 1;
          }
          else{
            nodo->FE = 0;
          }
          if(nodo2->FE = 1){
            nodo1->FE = -1;
          }
          else{
            nodo1->FE = 0;
          }

          nodo = nodo2;
          nodo2->FE = 0;
        }
        break;
    }
  }
  *raiz = nodo;
}


// Inserción del nodo en el subarbol correspondiente ya sea derecho o izquierdo
void Avl::insertarNodo(Nodo **raiz, int *altura, string ID) {
  // Variables que permiten a las funciones trabajar las condiciones
  // para generar un arbol balanceado
  Nodo *nodo = NULL;
  Nodo *nodo1 = NULL;
  Nodo *nodo2 = NULL;

  nodo = *raiz;

  if(nodo != NULL){
    if(ID < nodo->dato){
      // si el dato es menor se inserta a la izquierda (subarbol izquierdo)
      insertarNodo(&(nodo->izq), altura, ID);
      if(*altura == true){
        switch (nodo->FE){
          case 1:
            nodo->FE = 0;
            *altura = false;
            break;

          case 0:
            nodo->FE = -1;
            break;

          case -1:
            // Reestructuración del árbol
            nodo1 = nodo->izq;
            // Rotación II
            if(nodo1->FE <= 0){
              nodo->izq = nodo1->der;
              nodo1->der = nodo;
              nodo->FE = 0;
              nodo = nodo1;
            }
            else{
              // Rotación ID
              nodo2 = nodo1->der;
              nodo->izq = nodo2->der;
              nodo2->der = nodo;
              nodo1->der = nodo2->izq;
              nodo2->izq = nodo1;

              if(nodo2->FE == -1){
                nodo->FE = 1;
              }
              else{
                nodo->FE = 0;
              }
              if(nodo2->FE == 1){
                nodo1->FE = -1;
              }
              else{
                nodo1->FE = 0;
              }
              nodo = nodo2;
            }
            // nodo base tendra FE de 0
            nodo->FE = 0;
            *altura = false;
            break;
        }
      }

    }
    else {
        if(ID > nodo->dato){
          // Si es mayor, se inserta a la derecha (subarbol derecho)
          insertarNodo(&(nodo->der), altura, ID);
          if(*altura == true){
            switch (nodo->FE) {
              case -1:
                nodo->FE = 0;
                *altura = false;
                break;

              case 0:
                nodo->FE = 1;
                break;

              case 1:
                nodo1 = nodo->der;

                if(nodo1->FE >= 0){
                  nodo->der = nodo1->izq;
                  nodo1->izq = nodo;
                  nodo->FE = 0;
                  nodo = nodo1;
                }

                else{
                  nodo2 = nodo1->izq;
                  nodo->der = nodo2->izq;
                  nodo2->izq = nodo;

                  nodo1->izq = nodo2->izq;
                  nodo2->der = nodo1;

                  if (nodo2->FE == 1){
                    nodo->FE = -1;
                  }
                  else{
                    nodo->FE = 0;
                  }
                  if (nodo2->FE == -1){
                    nodo1->FE = 1;
                  }
                  else{
                    nodo1->FE = 0;
                  }
                  nodo = nodo2;
                }

                nodo->FE = 0;
                *altura = false;
                break;
            }
          }
        }

        else{
          // Menciona que el nodo ya se encuentra en el arbol
          cout << "Nodo se encuentra en el arbol" << endl;
        }
      }
    }
    else{
      // Se genera un nuevo nodo en caso de que el arbol este vacío
      nodo = new Nodo;
      nodo->izq = NULL;
      nodo->der = NULL;
      nodo->dato = ID;
      nodo->FE = 0;
      *altura = true;
  }
  *raiz = nodo;
}

// Borra un nodo específico, complemento de función eliminarNodo
void Avl::Borrar(Nodo **aux1, Nodo **otro1, int *altura){
  Nodo *aux;
  Nodo *otro;
  // auxiliares para búsqueda
  aux = *aux1;
  otro = *otro1;

  if(aux->der != NULL){
    Borrar(&(aux->der), &otro, altura);
    Reestructura_izq(&aux, altura);
  }
  else{
    otro->dato = aux->dato;
    aux = aux->izq;
    *altura = true;
  }
  *aux1 = aux;
  *otro1 = otro;
}

// Elimina nodos ordenadamente
void Avl::eliminarNodo(Nodo **raiz, int *altura, string ID){
  Nodo *temp;
  Nodo *nodo;

  nodo = *raiz;

  if(nodo != NULL){
    // Identifica el subarbol al que pertenece el dato
    if(ID < nodo->dato){
      // Recursiva para eliminar el dato
      eliminarNodo(&(nodo->izq), altura, ID);
      // Reestructura los datos por derecha
      Reestructura_der(&nodo, altura);
    }

    else{
      if(ID > nodo->dato){
        eliminarNodo(&(nodo->der), altura, ID);
        // Reestructura datos por izquierda
        Reestructura_izq(&nodo, altura);
    }

    else{
      temp = nodo;
      // Subarbol derecho vacío
      if(temp->der == NULL){
        // Va hacia la izquierda
        nodo = temp->izq;
        *altura = true;
      }

      else{
        // Subarbol izquierdo bacío
        if(temp->izq == NULL){
          // Va hacia la derecha
          nodo = temp->der;
          *altura = true;
        }

        else{
          Borrar(&(temp->izq), &temp, altura);
          Reestructura_der(&nodo, altura);
          // Libera info
          free(temp);
          }
        }
      }
    }
  }

  else{
    cout << "El nodo NO se encuentra en el árbol\n" << endl;
  }
  *raiz = nodo;
}

// Busca un nodo en el arbol
void Avl::Buscar_nodo(Nodo *nodo, string ID){
  if(nodo != NULL){
    if(ID < nodo->dato){
      // Si es menor se buca por la izquierda
      Buscar_nodo(nodo->izq, ID);
    }
    else{
      if(ID > nodo->dato){
        // Si es mayor se busca por la derecha
        Buscar_nodo(nodo->der, ID);
      }
      else{
        cout << "El nodo está en el arbol" << endl;
      }
    }
  }
    else{
      cout << "El nodo no está en el arbol" << endl;
    }
}

// Carga algunas IDs de los archivos de PBD
void Avl::Leer_algunos(Nodo **raiz, int *altura){
  string ID_prot;

  ifstream read("algunas.txt");

  while(!read.eof()){
    read >> ID_prot;
    this->insertarNodo(raiz, altura, ID_prot);
  }
  read.close();
}

// Carga todos los IDs de PDB
void Avl::Leer_todos(Nodo **raiz, int *altura){
  string ID_prots;

  ifstream read("Idsproteins.txt");

  while(!read.eof()){
    read >> ID_prots;
    this->insertarNodo(raiz, altura, ID_prots);
  }
  read.close();
}
