#include <iostream>
// Definir el nodo
#include "programa.h"

#ifndef ARBOL_H
#define ARBOL_H

class Avl{
    private:
      Nodo *nodotemp = NULL;

    public:
      Nodo *raiz = NULL;
      Avl();
      Nodo* crea_Nodo(string ID);
      void insertarNodo(Nodo **raiz,int *altura, string ID);
      void eliminarNodo(Nodo **raiz, int *altura, string ID);
      void Reestructura_der(Nodo **raiz, int *altura);
      void Reestructura_izq(Nodo **raiz, int *altura);
      void Borrar(Nodo **aux1, Nodo **otro1, int *altura);
      void Buscar_nodo(Nodo *nodo, string ID);
      void Leer_algunos(Nodo **raiz, int *altura);
      void Leer_todos(Nodo **raiz, int *altura);
};
#endif
