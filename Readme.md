# Arboles Balanceados (AVL)
Previamente se entregó un código en lenguaje C y se solicitó traspasarlo a C++, el código consistía en la generación
de un árbol balanceado ingresando datos de tipo entero (números), el cual posteriormente se adaptó al lenguaje C++,
acpetando el ingreso de ID de proteínas de la base de datos PDB. Se solicita que el programa almacene los ID de 
aproximadamente 170000 proteinas,además efectuando operaciones básicas en este, como: insertar ID, eliminar ID, 
modificar un elemento (eliminar dato viejo e insertar uno nuevo) y generar un grafo de la estructura creada mostrando
además de la relación entre los nodos y sus valores, el correspondiente y el factor de equilibrio (FE) asociado a cada nodo.

# Como funciona
El programa inicia con un menú general en el cual se presentan 7 situaciones posibles de interacción en relación a la inserción de
IDs de proteinas, estas fueron entregadas en 7 archivos de texto los cuales se unieron en un único gran archivo. Las opciones de
interacción permitidas son las siguientes. 

-Opción 1: Corresponde a la inserción de un nuevo ID de proteina.

-Opción 2: Permite al usuario eliminar un ID que contenga el arbol.

-Opción 3: Modificar un ID, eliminando uno ya existente e insertando otro nuevo.

-Opción 4: Permite leer un archivo de texto el cual se denomina algunas.txt- Este archivo contiene 40 IDs de los distintos archivos
a manera de generar una pequeña demostración de la creación del grafico.

-Opción 5: Permite cargar todos los IDs de proteinas del archivo txt creado.

-Opción 6: Genera el grafo correspondiente a las proteinas, sin embargo al momento de ingresar ID manuelmente no presenta complicaciones,
de lo contrario es el caso de leer algun archivo ya sea el pequeño o el enorme, presentando ciertas disfunciones, las cuales no se lograron 
modificar sin complicaciones.

-Opción 7: Permite al usuario finalizar el programa.

# Requisitos pŕevios
Graphviz.
   Para instalar Graphviz debe usar los comandos: 
	sudo apt-get install graphviz
	sudo apt-get update

# Para ejecutar
Se debe descargar la carpeta con los respectivos archivos .cpp y .h, luego abrir la terminal en la carpeta donde
se almacenaron los archivos. Prontamente se debe ejecutar el comando make en la terminal para la respectiva compilación 
de los archivos y luego ejecutar el comando ./programa para iniciar el programa y su interacción.

# Construido con 

- sistema operativo (SO): Ubuntu

- lenguaje de programación: C++

- libreria(s): iostream, fstream

- editor de texto: Atom

# Versiones

- Ubuntu 20.04 LTS

- Atom 1.52.0

# Autor

- Martín Muñoz Vera 
